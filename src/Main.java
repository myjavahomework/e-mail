import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class Main {
    public static void main(String[] args) {
        System.out.println(test("nick@mail.com"));      //true
        System.out.println(test("  BACON"));    //false
        System.out.println(test("BACON  "));    //false
        System.out.println(test("^BACON$"));    //false
        System.out.println(test("bacon"));      //false

        }
    public static boolean test(String testString){
        Pattern p = Pattern.compile("^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$");
        Matcher m = p.matcher(testString);
        return m.matches();

    }
}
//' [A-Za-z0-9]){1,}[\\-]{0,1}
       // /^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i